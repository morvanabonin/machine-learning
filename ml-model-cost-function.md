## Machine Learning - Model e Cost Function

### Model Representation

o modelo de representação da Figura 1 mostra a seguinte notação, para valores de entrada tem-se $`x`$ e para os valores de saída o $`y`$. O par $`x`$  e $`y`$  é chamado de exemplo de treinamento


##### Figura 1 - Exemplo de um modelo de representação
![Model Representation](/images/process-model-representation.png)

#### Referências:
[Machine Learning](https://www.coursera.org/learn/machine-learning)