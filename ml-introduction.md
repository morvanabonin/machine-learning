## Machine Learning - Introdução

De acordo a definição de Arthur Samuel (1959), Machine Learning é um campo de estudos que da as máquinas a habilidade de aprender sem ser  explicitamente programada.
Uma recente definição feita por Tom Mitchell diz que: "É dito a um programa de computador para aprender com a experiência E com relação a alguma classe de tarefas T e medida pelo desempenho P, seu desempenho em tarefas T, como medido P, melhora com a experiência E."

Exemplo: jogando damas.

E = a experiência de jogar muitos jogos de damas

T = a tarefa de jogar damas.

P = a probabilidade de que o programa vença o próximo jogo.

Existem vários algoritmos em Machine Learning, os dois principais são _Supervised Learning_ e _Unsupervised Learning_, também existem _Semi-supervised Learning_ e _Reinforcement Learning_.

### Supervised Learning
A maioria das práticas de Machine Learning usam _Supervised Learning_. Em _Supervised Learning_ tem-se as variaveis de entrada e de saída e o algoritmo aprende mapeando através delas, ou seja, você já sabe qual seria a saída correta.
_Supervised Learning_ o padrão de aprendizado se concentra do conjunto de dados e a decodificação da relação entre as entradas (variáveis independentes) e suas conhecidas saídas (variáveis dependentes). Uma variável independente (geralmente expressada como "_x_") é a variável que supostamente impacta a variável dependente (geralmente expressada como "_y_"). Por exemplo, o suplemento de oléo (_X_) impacta no custo do combustível (_Y_).

Os problemas em _Supervised Learning_ podem ser categorizados em dois grupos _regression_ e _classifiction_.
 - _regression_ -  Em um problema de _regression_, a predição acontece através dos resultados de uma saa contínua, isso significa que tenta-se mapear variáveis de entrada para alguma função contínua.
 - _classifiction_ - Em um problema de  _classifiction_, a predição dos resultados é feita a partir de uma saída discreta. Em outras palavras, tenta-se mapear variáveis de entrada em categorias distintas.

 Exemplos de algoritmos usados para _supervised learning_ incluem _regression analysis_, _decision trees_, _k-nearest neighbors_, _neural networks_, e máquina de vetores de suporte.

### Unsupervised Learning
Em _Unsupervised Learning_, tem-se a entrada de dados, mas não se tem a ideia do resultado. O objetivo é poder derivar estrutura de dados onde não necessariamente sabe-se o efeito das variáveis, em suma, é modelar a estrutura ou distribuição subjacente nos dados para aprender mais sobre os mesmos.

No caso do _Unsupervised Learning_ o foco é analisar as relações entre as variáveis de entrada e descobrir o padrão escondido que pode ser extraído e criar novas possíveis saídas. A vantagem do uso de _unsupervised learning__ é a possibilidade de descobrir novos padrões nos daddos que não se sabia que existia.

Os problemas em _Unsupervised Learning_ podem ser categorizados em dois grupos _clustering_ e _non-clustering_.
 - _clustering_ - utiliza-se para descobrir grupos em um determinado _dataset_.
 - _non-clustering_ - utiliza-se para descobrir estruturas de dados em um ambiente caótico.

 Exemplos do uso de _unsupervised learning_ algoritmos incluem _association analysis_, _social network analysis_ e _descending dimension algorithms_.


#### Referências:
[What’s the Difference Between Supervised, Unsupervised, Semi-Supervised and Reinforcement Learning?](https://blogs.nvidia.com/blog/2018/08/02/supervised-unsupervised-learning)  
[Supervised and Unsupervised Machine Learning Algorithms](https://machinelearningmastery.com/supervised-and-unsupervised-machine-learning-algorithms)  
[Machine Learning](https://www.coursera.org/learn/machine-learning)  
[Machine Learning For Beginners](https://towardsdatascience.com/machine-learning-for-beginners-d247a9420dab)  
[Machine Learning For Absolute Beginners - Book](https://www.amazon.com/Machine-Learning-Absolute-Beginners-Introduction-ebook/dp/B07335JNW1)
